\renewcommand{\FIGREP}{src/compilation_basics/figures}

\section{Compilation}
\label{sec:compilation}
\intersec{deneb}

\subsection{The big picture}
\label{sec:big-picture}
\begin{frame}
  \frametitle{Compilation}
  \framesubtitle{0100101110101001010...}
  \begin{itemize}
    \item A machine code: ON and OFF states (1 and 0)
    \item Binary code not so human readable
    \item Us of higher languages, e.g. C, C++, Fortran
    \item We need a translator!
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Compilation}
  \framesubtitle{The four compilation steps}
  \begin{itemize}
    \item Translation is made by a compiler in 4 steps
    \begin{description}
      \item[Preprocessing] Format source code to make it ready for compilation (remove comments, execute preprocessing directives such as \cmd{\#include}, \etc{})
      \item[Compiling] Translate the source code (C, C++, Fortran, \etc{}) into a low level language
      \item[Assembly] Translate the low level language into machine code and store it in object files
      \item[Linking] Link all the object files into one executable
    \end{description}
    \item In practice, the first three steps are combined together and simply
          called ``compiling''
  \end{itemize}
\end{frame}

\begin{frame}[t,fragile]
  \frametitle{Compilation}
  \framesubtitle{The four compilation steps (visually)}
  \hspace{5cm}
  \begin{minipage}{0.6\textwidth}
    \begin{itemize}
      \item<5> Note that in reality, everything is done transparently
            \begin{bashcode}
              $> gcc -c file_1.c
              $> gcc -c file_2.c
              $> gcc file_1.o file_2.o -lexample -o exec
            \end{bashcode}%$
    \end{itemize}
  \end{minipage}

  \onslide<1>\addimage[width=12cm]{\FIGREP/compilation_steps_0.pdf}{2cm}{1cm}
  \onslide<2>\addimage[width=12cm]{\FIGREP/compilation_steps_1.pdf}{2cm}{1cm}
  \onslide<3>\addimage[width=12cm]{\FIGREP/compilation_steps_2.pdf}{2cm}{1cm}
  \onslide<4>\addimage[width=12cm]{\FIGREP/compilation_steps_3.pdf}{2cm}{1cm}
  \onslide<5>\addimage[width=12cm]{\FIGREP/compilation_steps_4.pdf}{2cm}{1cm}
\end{frame}

\subsection{Basics of compilation}
\label{sec:basics-compilation}
\begin{frame}
  \frametitle{Compilation}
  \framesubtitle{Let's start from the beginning}

  \begin{itemize}
    \item Different compilers exists for different languages (here C, C++, and
    Fortran)
    \begin{itemize}
      \item GNU (\cmd{gcc}, \cmd{g++}, \cmd{gfortran})
      \item Clang/LLVM (\cmd{clang}, \cmd{clang++})
      \item Intel (\cmd{icc}, \cmd{icpc}, \cmd{ifort}) and Intel OneAPI (\cmd{icx}, \cmd{icpx}, \cmd{ifx})
      \item NVIDIA (\cmd{nvc}, \cmd{nvc++}, \cmd{nvfortran})
      \item IBM (\cmd{xlc}, \cmd{xlc++}, \cmd{xlf})
      \item \etc{}
    \end{itemize}
    \item Each vendor has specific options and usage
  \end{itemize}
  \vfill
  \textbf{In the following, we will use GNU compilers, but everything can be
    adapted to other vendors}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Compiling a single source file}
  \framesubtitle{}

  \begin{itemize}
    \item In general, the command to compile a single source file is
          \begin{bashcode}
            <compiler> <compiling options> <source file> <linking options>
          \end{bashcode}
    \item For example
          \begin{bashcode}
            g++ -Wall code.cc -o my_exe
          \end{bashcode}
          I want to compile the file \cmd{code.cc} using the GNU C++ compiler and with options:
          \begin{itemize}
            \item \cmd{-Wall} add more warning during compilation
            \item \cmd{-o my\_exe} rename the executable to \cmd{my\_exe} instead
                  of the default \cmd{a.out}
          \end{itemize}
    \item We will look at the linking later in the course
  \end{itemize}
\end{frame}

\subsection{Compilation options}
\label{sec:compilation-options}
\begin{frame}
  \frametitle{Compilation}
  \framesubtitle{Compilation options}
  Many compilation options can be used to manipulate, optimize or debug such as:
  \begin{itemize}
    \item "Manipulation": \cmd{-o}, \cmd{-c}, \etc{}
    \item "Optimization": \cmd{-O<n>}, \cmd{-fastmath}, \etc{}
    \item "Debug": \cmd{-g}, \cmd{-Wall}, \cmd{-fcheck=all} \etc{}
    \item Option summary for GNU: \url{https://gcc.gnu.org/onlinedocs/gcc/Invoking-GCC.html\#Invoking-GCC}
  \end{itemize}
\end{frame}

\begin{frame}[exercise]
  \frametitle{Compilation}
  \framesubtitle{Exercise Simple Compilation}
  The following code performs a simple daxpy, \ie{} $\vec{y} = \alpha \vec{x} + \vec{y}$
  \begin{itemize}
    \item Go to the directory \cmd{simple_compilation}
    \item Compile the code \cmd{daxpy.\{F90,c\}} with no options and execute it. What
          happens?
    \item Make the code compile with
          \begin{itemize}
            \item \cmd{-g -Wall -Wextra -Werror -fcheck=all} (Fortran)
            \item \cmd{-g -Wall -Wextra -Werror -fsanitize=address} (C/C++)
          \end{itemize}
          and run it. What happens?
    \item Try different compilation optimization options (\cmd{-O0} to
          \cmd{-O3}) and compare the timing displayed from the corresponding
          runs
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Discussion}
  \framesubtitle{Exercise simple\_compilation}
  \begin{itemize}
    \item Compilers greatly help you to debug and optimize your code. Use them
          properly!
    \item We advise you to begin by disabling optimizations and activating warnings
          \begin{itemize}
            \item \cmd{-g}: add debug information
            \item \cmd{-Wall -Wextra}: activate most warnings, and extra ones (still not everything...)
            \item \cmd{-Werror}: treat warnings as errors
            \item \cmd{-fcheck=all}, \cmd{-fsanitize=address}: enable run-time checks
          \end{itemize}
    \item Some errors can be easily avoided using the proper flags!
    \item Once everything is correct, remove those options and try optimization flags
    \item \cmd{-O3} is not necessarily faster than \cmd{-O2}. Test it!
  \end{itemize}
\end{frame}

% \begin{frame}[exercise]
%   \frametitle{Compilation}
%   \framesubtitle{Exercise compilationWith2Files}
%   \begin{itemize}
%     \item Go to the directory \cmd{compilationWith2Files}
%     \item The previous code has been split into two files: \cmd{main.\{F90,c\}}
%     and \cmd{saxpy.\{F90,c\}}
%     \item Compile and execute the code. Hint, use the \cmd{-c} option.
%     \item Check the results compared to the \cmd{simpleCompilation} exercise
%   \end{itemize}
% \end{frame}

% \begin{frame}
%   \frametitle{Discussion}
%   \framesubtitle{Exercise compilationWith2Files}
%   \begin{itemize}
%     \item With multiple source files, one must compile them separately and link
%     them together at the end
%     \item The \cmd{-c} option compiles, but does not link
%     \item Remember to pass all the options to each file!
%     \item This quickly becomes cumbersome as the number of files increases. We
%     will see possible solutions later.
%   \end{itemize}
% \end{frame}

\subsection{The preprocessor}
\label{sec:preprocessor}
\begin{frame}[t,fragile]
  \frametitle{Preprocessor}
  \framesubtitle{}

  \begin{itemize}
    \item The C preprocessor, \cmd{cpp}, is a tool that modifies the source code prior to
          compilation
    \item It treats all the lines starting with \code{\#}, \eg{}
          \begin{cxxcode}{}
            #include <iostream>
          \end{cxxcode}
          \pause
    \item You can define variables, macros, \etc{}
          \pause
          \begin{cxxcode}{}
            #define N=10
          \end{cxxcode}
          \pause
          \begin{cxxcode}{}
            #define max(x, y) ((x) > (y) ? (x) : (y))
          \end{cxxcode}
          \pause
    \item It can also take variables from the compilation command, \eg{}
          \begin{bashcode}
            $> gcc -DMYVAR code.c
          \end{bashcode}
          \pause
    \item By default, \cmd{gcc} calls the preprocessor before compiling, \cmd{gfortran}, you have to ask it:
          \begin{itemize}
            \item Use capital file extensions, \eg{} \cmd{.F90}, \cmd{.F}, \cmd{.F03}, \etc{}
            \item Add the \cmd{-cpp} option to the compiler
          \end{itemize}
  \end{itemize}
\end{frame}

% \begin{frame}[fragile, exercise]
%   \frametitle{Preprocessor}
%   \framesubtitle{}

%   \begin{itemize}
%     \item Compile \cmd{daxpy.\{F90,cc\}} as usual and run it
%     \item Compile \cmd{daxpy.\{F90,cc\}} by defining the \cmd{MY_MACRO} variable
%     and run it. What has happened?
%     \item For Fortran users, try changing the extension to \cmd{.f90} and make
%     the preprocessor work
%     \\emph{}nd{itemize}
%   \end{frame}

%%%   Local Variables:
%%%   mode: latex
%%%   TeX-master: "../../SCM_slides"
%%%   End:
