\renewcommand{\FIGREP}{src/mpi/figures}

\section{Compilation with MPI/OpenMP}
\label{sec:mpi}
\intersec{deneb}

\subsection{Introduction}
\label{sec:intro}

\begin{frame}
	\frametitle{Introduction}
	\framesubtitle{Terminology}
	\begin{itemize}
		\item Processes vs threads:
		      \begin{itemize}
			      \item Processes are what execute your program. They have resources
			            allocated specifically to them such as memories.
			      \item Threads are an execution unit for your program. This is typically
			            what executes instructions. There can be many per processes and they
			            share the resources of the process.
		      \end{itemize}
		      \pause
		      \vfill
		\item Shared Memory:
		      \begin{itemize}
			      \item In a shared memory model, all data is visible to all threads
			      \item All the threads can read and write to the same memory space, which
			            can be problematic if it happens at the same time!
			      \item Limited to one node
			      \item Multi-threading is often implemented using OpenMP (in HPC codes)
		      \end{itemize}
		      \pause
		      \vfill
		\item Distributed Memory:
		      \begin{itemize}
			      \item Many processes with distinct memory spaces. The processes need to
			            explicitly exchange data.
			      \item We can use many CPUs to increase the performance and/or problem size
			      \item All the largest HPC systems use such a distributed memory model.
			      \item The \textit{de facto} standard technology is MPI
		      \end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}[fragile,t]
	\frametitle{Introduction}
	\framesubtitle{MPI Programming model}
	% \begin{minipage}{0.49\linewidth}
	\begin{itemize}
		\item Parallel programming models exist as an abstraction above hardware and memory architectures
		\item These models are NOT specific to a particular type of machine or memory architecture
		\item In fact, any of these models can (theoretically) be implemented on any underlying hardware
		\item The performance depends on the binding of Processes/threads to CPU/cores
		\item Idealized NUMA (Non Uniform Memory Access) architecture
	\end{itemize}
	\addimage[width=6cm]{\FIGREP/DistributedSharedMemory}{5cm}{1cm}
	% \end{minipage}
\end{frame}

\subsection{MPI standard and implementations}
\label{sec:implementations}

\begin{frame}
	\frametitle{MPI implementations}
	\framesubtitle{}
	\begin{itemize}
		\item MPI is a standard defined by the MPI Forum committee\\
		      \url{https://www.mpi-forum.org/}
		\item Currently, MPI is at version 4.0
		\item \textit{De facto} standard for distributed memory
		\item The MPI Forum defines the standard and MPI vendors implement it
		\item There are many different implementations
		      \begin{itemize}
			      \item MPICH
			      \item MVAPICH (based on MPICH)
			      \item Intel MPI
			      \item OpenMPI
			      \item Platform MPI (IBM)
			      \item Etc.
		      \end{itemize}
	\end{itemize}
\end{frame}

\subsection{Compiling an MPI program}
\label{sec:compiling}
\begin{frame}
	\frametitle{Compiling MPI code}
	\framesubtitle{}
	\begin{itemize}
		\item MPI is just another library! You need to include and link it to your executable.
		\item Hopefully, vendors provide wrappers to make our life easier
		\item A few examples: \cmd{mpicc}, \cmd{mpiicc}, \cmd{mpicxx},
		      \cmd{mpiifort}, \cmd{mpif90}, \cmd{mpifort}, etc.
		\item Which one to use? It depends...
		      \begin{itemize}
			      \item In general, \cmd{cc} corresponds to C, \cmd{cxx} or \cmd{c++} to C++, and
			            \cmd{fort} or \cmd{fXY} to Fortran
			      \item Then, it depends on the vendor, \eg{}:
			            \begin{itemize}
				            \item For Intel MPI, it is better to use \cmd{mpiicc}, \cmd{mpiicxx},
				                  and \cmd{mpiifort}
				            \item OpenMPI advises you to use \cmd{mpicc}, \cmd{mpicxx},
				                  and \cmd{mpifort}
			            \end{itemize}
			      \item While you may have many wrappers available, look at the vendor's
			            documentation!
		      \end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{MPI wrappers}
	\framesubtitle{Example with Intel MPI}

	\begin{itemize}
		\item I would like to compile my C code with Intel compiler and MPI
		\item Let's use mpicc, it's available!
		      \begin{consoleoutput}
			      $> mpicc -show
			      gcc -I/.../impi/2018.4.274/intel64/include
			      -L/.../impi/2018.4.274/intel64/lib/release_mt
			      -L/.../impi/2018.4.274/intel64/lib
			      -Xlinker --enable-new-dtags
			      -Xlinker -rpath
			      -Xlinker /.../impi/2018.4.274/intel64/lib/release_mt
			      -Xlinker -rpath
			      -Xlinker /.../impi/2018.4.274/intel64/lib
			      -Xlinker -rpath
			      -Xlinker /opt/intel/mpi-rt/2107.0.0/intel64/lib/release_mt
			      -Xlinker -rpath
			      -Xlinker /opt/intel/mpi-rt/2017.0.0/intel64/lib
			      -lmpifort -lmpi -lmpigi -ldl -lrt -lpthread
		      \end{consoleoutput}%$
		      \pause
		\item It uses \cmd{gcc}!
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{MPI wrappers}
	\framesubtitle{Example with Intel MPI cont.}

	\begin{itemize}
		\item Let's try \cmd{mpiicc} now
		      \begin{consoleoutput}
			      $> mpiicc -show
			      icc -I/.../impi/2018.4.274/intel64/include
			      -L/.../impi/2018.4.274/intel64/lib/release_mt
			      -L/.../impi/2018.4.274/intel64/lib
			      -Xlinker --enable-new-dtags
			      -Xlinker -rpath
			      -Xlinker /.../impi/2018.4.274/intel64/lib/release_mt
			      -Xlinker -rpath
			      -Xlinker /.../impi/2018.4.274/intel64/lib
			      -Xlinker -rpath
			      -Xlinker /opt/intel/mpi-rt/2017.0.0/intel64/lib/release_mt
			      -Xlinker -rpath
			      -Xlinker /opt/intel/mpi-rt/2017.0.0/intel64/lib
			      -lmpifort -lmpi -lmpigi -ldl -lrt -lpthread
		      \end{consoleoutput}%$
		      \pause
		\item Now it uses \cmd{icc}!
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{How to use the wrappers in practice}
	\framesubtitle{}

	\begin{itemize}
		\item MPI wrapper invocation is very similar to a compiler one\\
		      \cmd{<MPI wrapper> <compiler options> <files to compile>}
		\item For example\\
		      \cmd{mpif90 -g -O3 my_code.f90}
		\item It is as easy as changing the compiler name by the MPI wrapper name
		\item Keep in mind that behind the wrapper, there is a compiler $\rightarrow$ use the
		      correct options
	\end{itemize}
\end{frame}

\subsection{Executing an MPI program}
\label{sec:executing}
\begin{frame}
	\frametitle{Executing an MPI program}
	\framesubtitle{}
	\begin{itemize}
		\item MPI programs are a bit particular in the sense that we need to launch
		      many instances of them
		\item Something needs to manage them and the communications
		\item Vendors provide commands to do that for us:
		      \begin{itemize}
			      \item \cmd{mpirun -n <number of tasks> <program>}: Non standard, but
			            provided by many vendors
			      \item \cmd{mpiexec -n <number of tasks> <program>}: Standard MPI startup command
			      \item \cmd{srun -n <number of tasks> <program>}: Slurm command
		      \end{itemize}
		      \vfill\pause
		\item Which one to use? (preferred first)
		      \begin{itemize}
			      \item On a machine managed by Slurm: \cmd{srun}
			      \item \cmd{mpiexec} as it is standard
			      \item \cmd{mpirun} is nothing else available
		      \end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}[exercise]
  \frametitle{MPI}
  \framesubtitle{Exercise compilation\_with\_mpi}
  \begin{itemize}
    \item Go to the directory \cmd{compilation_with_mpi}
    \item Load a compiler and an MPI implementation using the \cmd{module} command
    \item Compile ``by hand'' and execute the code using \cmd{srun} and multiple MPI processes
    \item Compile using the cmake project
  \end{itemize}
\end{frame}

\subsection{Compiling an OpenMP program}
\label{sec:ompCompilation}
\begin{frame}
	\frametitle{Compiling an OpenMP program}
	\framesubtitle{}
	\begin{itemize}
		\item OpenMP support comes from the standard compiler
		\item Syntax is compiler dependent
		      \begin{itemize}
			      \item \cmd{gcc -fopenmp mycode.c}
			      \item \cmd{gfortran -fopenmp mycode.f95}
			      \item \cmd{icx -fopenmp mycode.c}
			      \item \cmd{ifortx -fopenmp mycode.f95}
		      \end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}[t,fragile]
	\frametitle{Compiling an OpenMP program}
	\framesubtitle{}
	\begin{itemize}
		\item Example of slurm script to launch hybrid MPI+OpenMP code
		      \begin{bashcode}
			      #!/bin/bash

			      #SBATCH --nodes=1
			      #SBATCH --ntasks=6
			      #SBATCH --cpus-per-task=4
			      #SBATCH --mem=100G

			      module purge
			      module load intel intel-mpi
			      export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

			      srun my_hybrid_code.x --in=myinput.dat
		      \end{bashcode}
	\end{itemize}
\end{frame}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../SCM_slides"
%%% End:
