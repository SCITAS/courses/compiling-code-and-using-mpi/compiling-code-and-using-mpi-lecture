
.PHONY: all clean compilation_simple compilation_with_cmake compilation_with_cmake_openblas compilation_with_library compilation_with_makefile compilation_with_makefile_advanced compilation_with_mpi compilation_with_multiple_files libraries

all: compilation_simple compilation_with_cmake compilation_with_cmake_openblas compilation_with_library compilation_with_makefile compilation_with_makefile_advanced compilation_with_mpi compilation_with_multiple_files libraries

compilation_simple:
	make -C $@ -f .makefile

compilation_with_multiple_files:
	make -C $@ -f .makefile

compilation_with_makefile:
	make -C $@

compilation_with_makefile_advanced:
	make -C $@

compilation_with_mpi:
	cmake -S $@ -B $@/build
	cmake --build $@/build

libraries:
	make -C $@ -f .makefile

compilation_with_cmake:
	cmake -S $@ -B $@/build
	cmake --build $@/build

compilation_with_cmake_openblas:
	cmake -S $@ -B $@/build
	cmake --build $@/build

clean:
	make -C compilation_simple -f .makefile clean
	make -C compilation_with_multiple_files -f .makefile clean
	make -C compilation_with_makefile clean
	make -C compilation_with_makefile_advanced clean
	make -C libraries -f .makefile clean
	rm -rf compilation_with_mpi/build
	rm -rf compilation_with_cmake/build
	rm -rf compilation_with_cmake_openblas/build
