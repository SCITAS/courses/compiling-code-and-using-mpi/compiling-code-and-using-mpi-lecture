CXX ?= g++
CXXFLAGS += -Imy_math_library/include -O3
LDFLAGS += -Lmy_math_library/lib -lmymath

all: main

main: main_correct.o
	make -C my_math_library all
	$(CXX) $(CXXFLAGS) $? -o $@ $(LDFLAGS)

.PHONY: clean

clean:
	make -C my_math_library clean
	rm -rf *.o main
