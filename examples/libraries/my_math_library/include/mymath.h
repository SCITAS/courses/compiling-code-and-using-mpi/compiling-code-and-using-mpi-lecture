#ifndef MYMATH_H
#define MYMATH_H

// clang-format off

#if defined(__cplusplus)
extern "C"
#endif

double squared(double x);

#endif // MYMATH_H
