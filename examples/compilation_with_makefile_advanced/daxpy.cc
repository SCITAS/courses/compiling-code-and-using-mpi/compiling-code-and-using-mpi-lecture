#include "daxpy.hh"
#include <algorithm>
#include <assert.h>
#include <vector>

void daxpy(double a, const std::vector<double> &x, std::vector<double> &y) {
  assert(x.size() == y.size() && "The vector sizes do not match");

  for (size_t i = 0; i < y.size(); i++) {
    y[i] = a * x[i] + y[i];
  }
}

double max_value(const std::vector<double> &x) {
  assert(not x.empty() && "The vector is empty");

  auto max_it = std::max_element(x.begin(), x.end());

  return *max_it;
}
