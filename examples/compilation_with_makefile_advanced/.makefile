CXX ?= g++
CXXFLAGS += -Wall -Wextra -Werror -O3
EXEC = daxpy
OBJS = main.o daxpy.o

all: $(EXEC)

$(EXEC): $(OBJS)
	$(CXX) $(CXXFLAGS) $? -o $@ $(LDFLAGS)

%.o: %.cc
	$(CXX) $(CXXFLAGS) $< -c

.PHONY: clean all

clean:
	rm -rf $(OBJS) $(EXEC)
