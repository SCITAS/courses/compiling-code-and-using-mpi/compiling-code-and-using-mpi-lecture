CC ?= gcc
CXX ?= g++
F90 ?= gfortran

OPTIM = -Wall -Wextra -Werror -O3 -fsanitize=address

CFLAGS += $(OPTIM)
CXXFLAGS += $(OPTIM)
F90FLAGS += $(OPTIM)


all: daxpy_c daxpy_cxx daxpy_f90

daxpy_cxx: main_cxx.o daxpy_cxx.o
	$(CXX) $(CXXFLAGS) $? -o $@ $(LDFLAGS)

daxpy_c: main_c.o daxpy_c.o
	$(CC) $(CFLAGS) $? -o $@ $(LDFLAGS)

daxpy_f90: main_f90.o daxpy_mod_f90.o
	$(F90) $(CFLAGS)  $? -o $@ $(LDFLAGS)

main_f90.o: main.F90 daxpy_mod_f90.o
	$(F90) $(F90FLAGS) $< -c -o $@

%_c.o: %.c
	$(CC) $(CFLAGS) $< -c -o $@

%_cxx.o: %.cc
	$(CXX) $(CXXFLAGS) $< -c -o $@

%_f90.o: %.F90
	$(F90) $(F90FLAGS) $< -c -o $@

.PHONY: clean

clean:
	rm -rf *.o daxpy_c daxpy_cxx daxpy_f90
