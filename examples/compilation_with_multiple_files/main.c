#include "daxpy.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
  int i;
  const int n = 100000000;
  double a = 2.0;
  double *x = (double *)malloc(n * sizeof *x);
  double *y = (double *)malloc(n * sizeof *y);
  clock_t start_time, end_time;

  /* Init */
  for (i = 0; i < n; i++) {
    x[i] = 1.0;
    y[i] = 2.0;
  }

  start_time = clock();

  /* Call saxpy */
  daxpy(n, a, x, y);

  end_time = clock();
  printf("timing for daxpy C: %f ms\n",
         ((double)(end_time - start_time) / CLOCKS_PER_SEC) * 1000.);
  printf("max value of z: %f\n", max_value(n, y));
}
