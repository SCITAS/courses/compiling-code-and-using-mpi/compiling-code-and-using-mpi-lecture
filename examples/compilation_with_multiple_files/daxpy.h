#ifndef DAXPY_H_
#define DAXPY_H_

void daxpy(int n, double a, double *x, double *y);
double max_value(int n, double *x);

#endif // DAXPY_H_
