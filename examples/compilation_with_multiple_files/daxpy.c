#include "daxpy.h"

void daxpy(int n, double a, double *x, double *y) {
  for (int i = 0; i < n; i++) {
    y[i] = a * x[i] + y[i];
  }
}

double max_value(int n, double *x) {
  double max = x[0];

  for (int i = 1; i < n; ++i) {
    if (x[i] > max) {
      max = x[i];
    }
  }
  return max;
}
