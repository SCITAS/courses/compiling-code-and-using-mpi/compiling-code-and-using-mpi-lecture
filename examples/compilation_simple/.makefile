CC ?= gcc
CXX ?= g++
F90 ?= gfortran

FLAGS = -Wall -Wextra -Werror -O3 -fsanitize=address

CFLAGS += $(FLAGS)
CXXFLAGS += $(FLAGS)
F90FLAGS += $(FLAGS)


all: daxpy_c daxpy_cxx daxpy_f90

daxpy_cxx:  daxpy_cxx.o
	$(CXX) $(CXXFLAGS) $? -o $@ $(LDFLAGS)

daxpy_c:  daxpy_c.o
	$(CC) $(CFLAGS) $? -o $@ $(LDFLAGS)

daxpy_f90: daxpy_f90.o
	$(F90) $(CFLAGS)  $? -o $@ $(LDFLAGS)

%_c.o: %.c
	$(CC) $(CFLAGS) $< -c -o $@

%_cxx.o: %.cc
	$(CXX) $(CXXFLAGS) $< -c -o $@

%_f90.o: %.F90
	$(F90) $(F90FLAGS) $< -c -o $@

.PHONY: clean

clean:
	rm -rf *.o daxpy_c daxpy_cxx daxpy_f90
