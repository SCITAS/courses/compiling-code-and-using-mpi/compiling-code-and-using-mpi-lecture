#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void daxpy(int n, double a, double *x, double *y) {
  for (int i = 0; i < n; i++) {
    y[i] = a * x[i] + y[i];
  }
}

double max_value(int n, double *x) {
  double max = x[0];

  for (int i = 1; i < n; ++i) {
    if (x[i] > max) {
      max = x[i];
    }
  }
  return max;
}

int main() {
  const int n = 100000000;
  double a = 2.0;
  double *x = (double *)malloc(n * sizeof *x);
  double *y = (double *)malloc(n * sizeof *y);
  clock_t start_time, end_time;

  /* Init */
  for (int i = 0; i < n; i++) {
    x[i] = 1.0;
    y[i] = 2.0;
  }

  start_time = clock();

  /* Call daxpy */
  daxpy(n, a, x, y);

  end_time = clock();

  printf("timing for saxpy C: %f ms\n",
         ((double)(end_time - start_time) / CLOCKS_PER_SEC) * 1000.);
  printf("max value of z: %f\n", max_value(n, y));

  free(x);
  free(y);
}
