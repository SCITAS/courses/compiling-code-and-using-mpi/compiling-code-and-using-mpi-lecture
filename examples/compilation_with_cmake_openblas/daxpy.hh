#ifndef DAXPY_H_
#define DAXPY_H_

#define BLASFUNC(FUNC) FUNC##_

extern "C" {
int BLASFUNC(daxpy)(const int *, const double *, const double *, const int *,
                    double *, const int *);
}

int daxpy(const int n, const double alpha, const double * x, const int incx,
          double * y, const int incy) {
  return BLASFUNC(daxpy)(&n, &alpha, x, &incx, y, &incy);
}

#endif // DAXPY_H_
