! Main program
program main
  use iso_fortran_env, only: di=>int64, dp=>real64
  use saxpy_mod
  integer nmax 
  parameter (nmax=1000000)
  real(dp) x(nmax), y(nmax), z(nmax)
  real(dp) rate
  integer(di) :: startc, endc

  z=0._dp
  x=1._dp
  y=2._dp
  
  call system_clock(count_rate=rate)
  call system_clock(startc)
  call saxpy(nmax, 2.0_dp, x, y, z)
  call system_clock(endc)
  print*, "L2 norm of z = ",norm2(z)
  write(*,*) "timing for saxpy: ", real(endc-startc, dp)/rate*1000, "ms"
end program main
