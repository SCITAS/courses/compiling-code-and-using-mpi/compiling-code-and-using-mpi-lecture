#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <saxpy.h>

int main() {
  int i;
  const int n=100000000;
  double a=2.0;
  double* x = (double*) malloc(n*sizeof *x);
  double* y = (double*) malloc(n*sizeof *y);
  double* z = (double*) malloc(n*sizeof *z);
  clock_t start_time, end_time;

  /* Init */
  for (i = 0; i < n; i++){
    z[i] = 0.0;
    x[i] = 1.0;
    y[i] = 2.0;
  }

  start_time = clock();
  /* Call saxpy */
  saxpy(n,a,x,y,z);
  end_time = clock();
  printf ("timing for saxpy C: %f ms\n",((double)(end_time - start_time)/CLOCKS_PER_SEC)*1000.);
  printf ("max value of z: %f\n",maxValue(z,n));

}
