#include <vector>

#ifndef DAXPY_H_
#define DAXPY_H_

void daxpy(double a, const std::vector<double> &x, std::vector<double> &y);
double max_value(const std::vector<double> &x);

#endif // DAXPY_H_
